### How to Compile

``` bash
gcc -o client client.c
gcc -o server server.c
```

### How to Run

``` bash
./server
./client 127.0.0.1 /path/to/file
```
