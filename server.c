#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <sys/types.h>
#include <sys/fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "common.h"

#define QUEUE_SIZE 10
#define MAX_CLIENTS_NUM 100


enum bool {false, true};
typedef struct
{
    enum bool valid;
    int socketDescriptor;
    struct sockaddr_in addr;
} ClientInfo;

ClientInfo clientList[MAX_CLIENTS_NUM];
pthread_mutex_t mutexList;

void Fatal(char *string);
void InitClientList(ClientInfo *list);
int AddNewClient(ClientInfo *list, int socketDescriptor, struct sockaddr_in *addr);
void *ServerThread(void *param);

int main(int argc, char *argv[])
{
    int s, b, l, socketAccepted, on = 1;
    struct sockaddr_in channel;    /* holds IP address */
    pthread_t serverThreads[MAX_CLIENTS_NUM];
    pthread_attr_t attr;

    InitClientList(clientList);
    /* Build address structure to bind to socket. */
    memset(&channel, 0, sizeof(channel));    /* fill channel with 0 */
    channel.sin_family = AF_INET;  /* IPv4 Internet Protocol */
    channel.sin_addr.s_addr = htonl(INADDR_ANY);  /* converts the unsigned integer hostlong from host byte order to network byte order */
    channel.sin_port = htons(SERVER_PORT);  /* converts the unsigned short integer hostshort from host byte order to network byte order */

    /* Passive open. Wait for connection. */
    s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); /* create socket, AF for Address Family */
    if (s < 0)
        Fatal("socket failed");
    setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *) &on, sizeof(on));

    b = bind(s, (struct sockaddr *) &channel, sizeof(channel));
    if (b < 0)
        Fatal("bind failed");

    l = listen(s, QUEUE_SIZE);    /* specify queue size */
    if (l < 0)
        Fatal("listen failed");

    struct sockaddr_in clientAddr;
    size_t clientAddrLen;
    /* Socket is now set up and bound. Wait for connection and process it. */
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    while (1)
    {
        clientAddrLen = sizeof(clientAddr);
        socketAccepted = accept(s, (struct sockaddr *)&clientAddr, (socklen_t *)&clientAddrLen);  /* block for connection request */
        if (socketAccepted < 0)
            Fatal("accept failed");
#if DEBUG
        printf("Connected from client %s:%hu\n", inet_ntoa(clientAddr.sin_addr), ntohs(clientAddr.sin_port));
#endif
        int clientID = AddNewClient(clientList, socketAccepted, &clientAddr);
        if (clientID < 0)
        {
            Fatal("too many clients to handle");
        }
        pthread_create(&serverThreads[clientID], &attr, ServerThread, &clientList[clientID]);
#if 0
        int bytes, fd;
        char buf[BUF_SIZE];    /* buffer for outgoing file */
        read(socketAccepted, buf, BUF_SIZE); /* read file name from socket */

        /* Get and return the file. */
        fd = open(buf, O_RDONLY);  /* open the file to be sent back */
        if (fd < 0)
        {
            printf("%s\n", buf);
            Fatal("open failed");
        }
        while (1)
        {
            bytes = read(fd, buf, BUF_SIZE); /* read from file */
            if (bytes <= 0)  /* check for end of file */
                break;
            write(socketAccepted, buf, bytes);  /* write bytes to socket */
        }
        close(fd);  /* close file */
        close(socketAccepted);  /* close connection */
#endif
    }
}

void Fatal(char *string)
{
    printf("[ERROR] %s\n", string);
    exit(1);
}

void InitClientList(ClientInfo *list)
{
    int i;
    for (i = 0; i < MAX_CLIENTS_NUM; i++)
    {
        list[i].valid = false;
    }
}

int AddNewClient(ClientInfo *list, int socketDescriptor, struct sockaddr_in *addr)
{
    int i;
    for (i = 0; i < MAX_CLIENTS_NUM; i++)
    {
        if (true == list[i].valid)
        {
            continue;
        }
        memcpy(&list[i].addr, addr, sizeof(struct sockaddr_in));
        list[i].valid = true;
        list[i].socketDescriptor = socketDescriptor;
        return i;
    }
    return -1;  // error: the client list is full
}

void *ServerThread(void *param)
{
    ClientInfo *currentClient = param;
    char buf[BUF_SIZE];
    // say hello
    char greeting[] = "Hello.\n";
    write(currentClient->socketDescriptor, greeting, strlen(greeting) + 1);  /* write bytes to socket */

    //@TODO do something here

    // disconnect
    currentClient->valid = false;
    close(currentClient->socketDescriptor);  /* close connection */
    pthread_exit(0);
}
